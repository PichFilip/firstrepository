package core.model;

public enum PickupType {
    UNKNOW(-1), REGULAR(0), NO_PICKUP(1), PHONE_PICKUP(2), DRIVER_COORDINATE(3);
    private int type;

    PickupType(int type) {
        this.type = type;
    }

    public static PickupType getType(int type) {
        for (PickupType pickupType : PickupType.values()) {
            if (pickupType.type == type) {
                return pickupType;
            }
        }
        return UNKNOW;
    }
}

