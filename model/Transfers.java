package core.model;

public class Transfers extends FeedModel {
    public String fromStopId;
    public String toStopId;
    public TransferType transferType;
    public String transferTime;

    public Transfers() {
        super("");
        this.fromStopId = fromStopId;
        this.toStopId = toStopId;
        this.transferType = transferType;
        this.transferTime = transferTime;
    }

    public String getFromStopId() {
        return fromStopId;
    }

    public void setFromStopId(String fromStopId) {
        this.fromStopId = fromStopId;
    }

    public String getToStopId() {
        return toStopId;
    }

    public void setToStopId(String toStopId) {
        this.toStopId = toStopId;
    }

    public TransferType getTransferType() {
        return transferType;
    }

    public void setTransferType(TransferType transferType) {
        this.transferType = transferType;
    }

    public String getTransferTime() {
        return transferTime;
    }

    public void setTransferTime(String transferTime) {
        this.transferTime = transferTime;
    }

    @Override
    public String toString() {
        return "Transfers{" +
                "fromStopId='" + fromStopId + '\'' +
                ", toStopId='" + toStopId + '\'' +
                ", transferType=" + transferType +
                ", transferTime='" + transferTime + '\'' +
                '}';
    }
}
