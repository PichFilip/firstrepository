package core.model;

public class Route extends FeedModel{
    public String routeId;
    public String routeShortName;
    public String longName;
    public RouteType routeType;
    public String routeUrl;


    public Route() {
        super("");
        this.routeId = routeId;
        this.routeShortName = routeShortName;
        this.longName = longName;
        this.routeType = routeType;
        this.routeUrl = routeUrl;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getRouteShortName() {
        return routeShortName;
    }

    public void setRouteShortName(String routeShortName) {
        this.routeShortName = routeShortName;
    }

    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

    public RouteType getRouteType() {
        return routeType;
    }

    public void setRouteType(RouteType routeType) {
        this.routeType = routeType;
    }

    public String getRouteUrl() {
        return routeUrl;
    }

    public void setRouteUrl(String routeUrl) {
        this.routeUrl = routeUrl;
    }

    @Override
    public String toString() {
        return "Route{" +
                "routeId=" + routeId +
                ", routeShortName=" + routeShortName +
                ", longName='" + longName + '\'' +
                ", routeType=" + routeType +
                ", routeUrl='" + routeUrl + '\'' +
                '}';
    }
}
