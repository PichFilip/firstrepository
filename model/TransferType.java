package core.model;

import org.omg.CORBA.UNKNOWN;

public enum TransferType {
    UNKNOWN(-1), RECCOMENDED(0), STOP_TIME(1), NO_STOP_TIME(2), UNABLE(3);

    private int type;

    TransferType(int type) {
        this.type = type;
    }

    public static TransferType getType(int type) {
        for (TransferType transferType : TransferType.values()) {
            if (transferType.type == type){
                return transferType;
        }

    }
    return UNKNOWN;
}


}
