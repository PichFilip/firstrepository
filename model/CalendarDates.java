package core.model;

public class CalendarDates extends FeedModel {
    public String serviceId;
    public String date;
    public CalendarExceptionType calendarExceptionType;

    public CalendarDates() {
        super("");
        this.serviceId = serviceId;
        this.date = date;
        this.calendarExceptionType = calendarExceptionType;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public CalendarExceptionType getCalendarExceptionType() {
        return calendarExceptionType;
    }

    public void setCalendarExceptionType(CalendarExceptionType calendarExceptionType) {
        this.calendarExceptionType = calendarExceptionType;
    }

    @Override
    public String toString() {
        return "CalendarDates{" +
                "serviceId='" + serviceId + '\'' +
                ", date='" + date + '\'' +
                ", calendarExceptionType=" + calendarExceptionType +
                '}';
    }
}
