package core.model;

public class StopTimes extends FeedModel{
    private String tripId;
    private  String arrival;
    private String arrivalTime;
    private String departureTime;
    private String stopId;
    private String stopSequence;
    private String stopHeadsign;
    private PickupType pickupType;
    private PickupType dropOffType;

    public StopTimes() {
        super("");
    }

    public StopTimes(String id, String tripId, String arrival, String arrivalTime, String departureTime, String stopId, String stopSequence, String stopHeadsign, PickupType pickupType, PickupType dropOffType) {
        super(id);
        this.tripId = tripId;
        this.arrival = arrival;
        this.arrivalTime = arrivalTime;
        this.departureTime = departureTime;
        this.stopId = stopId;
        this.stopSequence = stopSequence;
        this.stopHeadsign = stopHeadsign;
        this.pickupType = pickupType;
        this.dropOffType = dropOffType;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartueTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getStopId() {
        return stopId;
    }

    public void setStopId(String stopId) {
        this.stopId = stopId;
    }

    public String getStopSequence() {
        return stopSequence;
    }

    public void setStopSequence(String stopSequence) {
        this.stopSequence = stopSequence;
    }

    public String getStopHeadsign() {
        return stopHeadsign;
    }

    public void setStopHeadsign(String stopHeadsign) {
        this.stopHeadsign = stopHeadsign;
    }

    public PickupType getPickupType() {
        return pickupType;
    }

    public void setPickupType(PickupType pickupType) {
        this.pickupType = pickupType;
    }

    public PickupType getDropOffType() {
        return dropOffType;
    }

    public void setDropOffType(PickupType dropOffType) {
        this.dropOffType = dropOffType;
    }

    @Override
    public String toString() {
        return "StopTimes{" +
                "tripId='" + tripId + '\'' +
                ", arrival='" + arrival + '\'' +
                ", arrivalTime='" + arrivalTime + '\'' +
                ", departueTime='" + departureTime + '\'' +
                ", stopId='" + stopId + '\'' +
                ", stopSequence='" + stopSequence + '\'' +
                ", stopHeadsign='" + stopHeadsign + '\'' +
                ", pickupType=" + pickupType +
                ", dropOffType=" + dropOffType +
                '}';
    }
}
