package core.model;

public class Calendar extends FeedModel{
    private String serviceId;
    private boolean mondayRuns;
    private boolean tuesdayRuns;
    private boolean wednesdayRuns;
    private boolean thursdayRuns;
    private boolean fridayRuns;
    private boolean saturdayRuns;
    private boolean sundayRuns;
    private String startDate;
    private String endDate;



    public Calendar() {
        super("");
        this.serviceId = serviceId;
        this.mondayRuns = mondayRuns;
        this.tuesdayRuns = tuesdayRuns;
        this.wednesdayRuns = wednesdayRuns;
        this.thursdayRuns = thursdayRuns;
        this.fridayRuns = fridayRuns;
        this.saturdayRuns = saturdayRuns;
        this.sundayRuns = sundayRuns;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getServiceId(String s) {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public boolean isMondayRuns() {
        return mondayRuns;
    }

    public void setMondayRuns(boolean mondayRuns) {
        this.mondayRuns = mondayRuns;
    }

    public boolean isTuesdayRuns() {
        return tuesdayRuns;
    }

    public void setTuesdayRuns(boolean tuesdayRuns) {
        this.tuesdayRuns = tuesdayRuns;
    }

    public boolean isWednesdayRuns() {
        return wednesdayRuns;
    }

    public void setWednesdayRuns(boolean wednesdayRuns) {
        this.wednesdayRuns = wednesdayRuns;
    }

    public boolean isThursdayRuns() {
        return thursdayRuns;
    }

    public void setThursdayRuns(boolean thursdayRuns) {
        this.thursdayRuns = thursdayRuns;
    }

    public boolean isFridayRuns() {
        return fridayRuns;
    }

    public void setFridayRuns(boolean fridayRuns) {
        this.fridayRuns = fridayRuns;
    }

    public boolean isSaturdayRuns() {
        return saturdayRuns;
    }

    public void setSaturdayRuns(boolean saturdayRuns) {
        this.saturdayRuns = saturdayRuns;
    }

    public boolean isSundayRuns() {
        return sundayRuns;
    }

    public void setSundayRuns(boolean sundayRuns) {
        this.sundayRuns = sundayRuns;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "Calendar{" +
                "serviceId='" + serviceId + '\'' +
                ", mondayRuns=" + mondayRuns +
                ", tuesdayRuns=" + tuesdayRuns +
                ", wednesdayRuns=" + wednesdayRuns +
                ", thursdayRuns=" + thursdayRuns +
                ", fridayRuns=" + fridayRuns +
                ", saturdayRuns=" + saturdayRuns +
                ", sundayRuns=" + sundayRuns +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                '}';
    }
}


