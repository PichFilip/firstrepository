package core.model;

import java.util.Objects;

public class Trip extends FeedModel {
    private String routeId;
    private String serviceId;
    private String tripId;
    private String tripHeadsign;
    private String directionId;
    private String blockId;
    private String shapeId;
    private boolean isWheelchairAccessible;
    private boolean isLowFloor;


    public Trip(){
        super("");
    }

    public Trip(String routeId, String serviceId, String tripId, String tripHeadsign, String directionId, String blockId, String shapeId, boolean isWheelchairAccessible, boolean isLowFloor) {
        super("");
        this.routeId = routeId;
        this.serviceId = serviceId;
        this.tripId = tripId;
        this.tripHeadsign = tripHeadsign;
        this.directionId = directionId;
        this.blockId = blockId;
        this.shapeId = shapeId;
        this.isWheelchairAccessible = isWheelchairAccessible;
        this.isLowFloor = isLowFloor;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getTripHeadsign() {
        return tripHeadsign;
    }

    public void setTripHeadsign(String tripHeadsign) {
        this.tripHeadsign = tripHeadsign;
    }

    public String getDirectionId() {
        return directionId;
    }

    public void setDirectionId(String directionId) {
        this.directionId = directionId;
    }

    public String getBlockId() {
        return blockId;
    }

    public void setBlockId(String blockId) {
        this.blockId = blockId;
    }

    public String getShapeId() {
        return shapeId;
    }

    public void setShapeId(String shapeId) {
        this.shapeId = shapeId;
    }

    public boolean isWheelchairAccessible() {
        return isWheelchairAccessible;
    }

    public void setWheelchairAccessible(boolean wheelchairAccessible) {
        isWheelchairAccessible = wheelchairAccessible;
    }

    public boolean isLowFloor() {
        return isLowFloor;
    }

    public void setLowFloor(boolean lowFloor) {
        isLowFloor = lowFloor;
    }

    @Override
    public String toString() {
        return "Trip{" +
                "routeId='" + routeId + '\'' +
                ", serviceId='" + serviceId + '\'' +
                ", tripId='" + tripId + '\'' +
                ", tripHeadsign='" + tripHeadsign + '\'' +
                ", directionId='" + directionId + '\'' +
                ", blockId='" + blockId + '\'' +
                ", shapeId='" + shapeId + '\'' +
                ", isWheelchairAccessible=" + isWheelchairAccessible +
                ", isLowFloor=" + isLowFloor +
                '}';
    }


    //TODO: Obczaj to w domu
    //w overreid mamy eqyuals i obiekt equals tworzy i wtedy uzywamy tego w treści i to jest milion linijek mnie jtekstu


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trip trip = (Trip) o;
        return isWheelchairAccessible == trip.isWheelchairAccessible &&
                isLowFloor == trip.isLowFloor &&
                Objects.equals(routeId, trip.routeId) &&
                Objects.equals(serviceId, trip.serviceId) &&
                Objects.equals(tripId, trip.tripId) &&
                Objects.equals(tripHeadsign, trip.tripHeadsign) &&
                Objects.equals(directionId, trip.directionId) &&
                Objects.equals(blockId, trip.blockId) &&
                Objects.equals(shapeId, trip.shapeId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(routeId, serviceId, tripId, tripHeadsign, directionId, blockId, shapeId, isWheelchairAccessible, isLowFloor);
    }
}
