package core.model;

public class Shapes extends FeedModel {
    public String shapeId;
    public String shapePtLat;
    public String shapePtLon;
    public String shapePtSequence;

    public Shapes() {
        super("");
        this.shapeId = shapeId;
        this.shapePtLat = shapePtLat;
        this.shapePtLon = shapePtLon;
        this.shapePtSequence = shapePtSequence;
    }

    public String getShapeId() {
        return shapeId;
    }

    public void setShapeId(String shapeId) {
        this.shapeId = shapeId;
    }

    public String getShapePtLat() {
        return shapePtLat;
    }

    public void setShapePtLat(String shapePtLat) {
        this.shapePtLat = shapePtLat;
    }

    public String getShapePtLon() {
        return shapePtLon;
    }

    public void setShapePtLon(String shapePtLon) {
        this.shapePtLon = shapePtLon;
    }

    public String getShapePtSequence() {
        return shapePtSequence;
    }

    public void setShapePtSequence(String shapePtSequence) {
        this.shapePtSequence = shapePtSequence;
    }

    @Override
    public String toString() {
        return "Shapes{" +
                "shapeId='" + shapeId + '\'' +
                ", shapePtLat='" + shapePtLat + '\'' +
                ", shapePtLon='" + shapePtLon + '\'' +
                ", shapePtSequence='" + shapePtSequence + '\'' +
                '}';
    }
}
