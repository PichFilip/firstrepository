package core.model;

public enum CalendarExceptionType {

    UNKNOW(0), ADDED(1), REMOVED(2);

    private int type;

    CalendarExceptionType(int type){
        this.type = type;
    }
    public static CalendarExceptionType getType(int type){
        for (CalendarExceptionType calendarExceptionType : CalendarExceptionType.values()){
            if (calendarExceptionType.type == type){
                return calendarExceptionType;
            }
        }
        return UNKNOW;
    }
}
