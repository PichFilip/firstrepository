package core.model;

public class Stops extends FeedModel {
    private String stopId;
    private String stopCode;
    private String stopName;
    private String stopDesc;
    private String stopLat;
    private String stopLon;
    private String stopUrl;
    private String locationType;
    private String parentStation;
    private boolean isWheelChairAccessible;

    public Stops() {
        super("");
    }

    public Stops(String id, String stopId, String stopCode, String stopName, String stopDesc, String stopLat, String stopLon, String stopUrl, String locationType, String parentStation, boolean isWheelChairAccessible) {
        super(id);
        this.stopId = stopId;
        this.stopCode = stopCode;
        this.stopName = stopName;
        this.stopDesc = stopDesc;
        this.stopLat = stopLat;
        this.stopLon = stopLon;
        this.stopUrl = stopUrl;
        this.locationType = locationType;
        this.parentStation = parentStation;
        this.isWheelChairAccessible = isWheelChairAccessible;
    }

    public String getStopId() {
        return stopId;
    }

    public void setStopId(String stopId) {
        this.stopId = stopId;
    }

    public String getStopCode() {
        return stopCode;
    }

    public void setStopCode(String stopCode) {
        this.stopCode = stopCode;
    }

    public String getStopName() {
        return stopName;
    }

    public void setStopName(String stopName) {
        this.stopName = stopName;
    }

    public String getStopDesc() {
        return stopDesc;
    }

    public void setStopDesc(String stopDesc) {
        this.stopDesc = stopDesc;
    }

    public String getStopLat() {
        return stopLat;
    }

    public void setStopLat(String stopLat) {
        this.stopLat = stopLat;
    }

    public String getStopLon() {
        return stopLon;
    }

    public void setStopLon(String stopLon) {
        this.stopLon = stopLon;
    }

    public String getStopUrl() {
        return stopUrl;
    }

    public void setStopUrl(String stopUrl) {
        this.stopUrl = stopUrl;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    public String getParentStation() {
        return parentStation;
    }

    public void setParentStation(String parentStation) {
        this.parentStation = parentStation;
    }

    public boolean isWheelChairAccessible() {
        return isWheelChairAccessible;
    }

    public void setWheelChairAccessible(boolean wheelChairAccessible) {
        isWheelChairAccessible = wheelChairAccessible;
    }

    @Override
    public String toString() {
        return "Stops{" +
                "stopId='" + stopId + '\'' +
                ", stopCode='" + stopCode + '\'' +
                ", stopName='" + stopName + '\'' +
                ", stopDesc='" + stopDesc + '\'' +
                ", stopLat='" + stopLat + '\'' +
                ", stopLon='" + stopLon + '\'' +
                ", stopUrl='" + stopUrl + '\'' +
                ", locationType='" + locationType + '\'' +
                ", parentStation='" + parentStation + '\'' +
                ", isWheelChairAccessible=" + isWheelChairAccessible +
                '}';
    }
}
