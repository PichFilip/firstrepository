package core.parser;

import core.model.Calendar;
import core.model.CalendarDates;
import core.model.CalendarExceptionType;

public class CalendarDatesParser extends BaseFeedParser<CalendarDates> {
    //service_id,date,exception_type
    private static final int SERVICE_ID_COLUMNE = 0;
    private static final int DATE_COLUMNE = 1;
    private static final int EXCEPTION_TYPE_COLUMNE = 2;

    @Override
    public CalendarDates parseTo(String line) {
        super.parseTo(line);
        CalendarDates calendarDates = new CalendarDates();
        calendarDates.setServiceId(splitFeedInfo[SERVICE_ID_COLUMNE]);
        calendarDates.setDate(splitFeedInfo[DATE_COLUMNE]);
        calendarDates.setCalendarExceptionType(CalendarExceptionType.getType(Integer.parseInt(splitFeedInfo[EXCEPTION_TYPE_COLUMNE])));

        return calendarDates;
    }
}
