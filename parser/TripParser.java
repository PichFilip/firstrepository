package core.parser;

import core.model.Trip;

public class TripParser extends BaseFeedParser<Trip>{
    private static final int TRIP_ROUTE_ID = 0;
    private static final int TRIP_SERVICE_ID = 1;
    private static final int TRIP_TRIP_ID = 2;
    private static final int TRIP_HEAD_SIGN = 3;
    private static final int TRIP_DIRECTION_ID = 4;
    private static final int TRIP_BLOCK_ID = 5;
    private static final int TRIP_SHAPE_ID = 6;
    private static final int TRIP_IS_WHEELCHAIR_ACCESSIBLE = 7;
    private static final int TRIP_IS_LOW_FLOOR = 8;

    @Override
    public Trip parseTo(String line) {
        super.parseTo(line);
        Trip trip = new Trip();
        trip.setRouteId(splitFeedInfo[TRIP_ROUTE_ID]);
        trip.setServiceId(splitFeedInfo[TRIP_SERVICE_ID]);
        trip.setTripId(splitFeedInfo[TRIP_TRIP_ID]);
        trip.setTripHeadsign(splitFeedInfo[TRIP_HEAD_SIGN]);
        trip.setDirectionId(splitFeedInfo[TRIP_DIRECTION_ID]);
        trip.setBlockId(splitFeedInfo[TRIP_BLOCK_ID]);
        trip.setShapeId(splitFeedInfo[TRIP_SHAPE_ID]);
        // WAZNE
        trip.setWheelchairAccessible(Integer.parseInt(splitFeedInfo[TRIP_IS_WHEELCHAIR_ACCESSIBLE]) == 1);
        trip.setLowFloor(Integer.parseInt(splitFeedInfo[TRIP_IS_LOW_FLOOR]) == 1);
        return trip;




    }
}
