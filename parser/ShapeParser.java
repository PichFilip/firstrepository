package core.parser;

import core.model.Shapes;

public class ShapeParser extends BaseFeedParser<Shapes>{
    //shape_id,shape_pt_lat,shape_pt_lon,shape_pt_sequence

    private static final int SHAPE_ID_COLUMNE = 0;
    private static final int SHAPE_PT_LAT_COLUMNE = 1;
    private static final int SHAPE_PT_LON_COLUMNE = 2;
    private static final int SHAPE_PT_SEQUENCE_COLUMNE = 3;

    @Override
    public Shapes parseTo(String line) {
        super.parseTo(line);
        Shapes shapes = new Shapes();
        shapes.setShapeId(splitFeedInfo[SHAPE_ID_COLUMNE]);
        shapes.setShapePtLat(splitFeedInfo[SHAPE_PT_LAT_COLUMNE]);
        shapes.setShapePtLon(splitFeedInfo[SHAPE_PT_LON_COLUMNE]);
        shapes.setShapePtSequence(splitFeedInfo[SHAPE_PT_SEQUENCE_COLUMNE]);
        return shapes;

    }
}
