package core.parser;

import core.model.Route;
import core.model.RouteType;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class RouteParser extends BaseFeedParser<Route>{
//"route_id","route_short_name","route_long_name","route_desc","route_type","route_url","route_color","route_text_color"
    private static final int ROUTE_ID_COLUMNE = 0;
    private static final int ROUTE_SHORT_NAME_COLUMNE = 1;
    private static final int ROUTE_LONG_NAME_COLUMNE = 2;
    private static final int ROUTE_TYPE_COLUMNE = 4;
    private static final int ROUTE_URL_COLUMNE = 5;

    @Override
    public Route parseTo(String line) {
        super.parseTo(line);
        Route route = new Route();
        route.setRouteId(splitFeedInfo[ROUTE_ID_COLUMNE]);
        route.setRouteShortName(splitFeedInfo[ROUTE_SHORT_NAME_COLUMNE]);
        route.setLongName(splitFeedInfo[ROUTE_LONG_NAME_COLUMNE]);
        route.setRouteType(RouteType.getType(Integer.parseInt(splitFeedInfo[ROUTE_TYPE_COLUMNE])));  //to nie jest string tylko pobieramy wartosc z enuma
        route.setRouteUrl(splitFeedInfo[ROUTE_URL_COLUMNE]);
        return route;
    }

}
