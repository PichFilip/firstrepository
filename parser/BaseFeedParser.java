package core.parser;

import core.model.FeedModel;

import java.io.File;

public abstract class BaseFeedParser<T extends FeedModel> implements FeedParser<T> {
    protected String[] splitFeedInfo;
   public BaseFeedParser() {super();}



    @Override
    public T parseTo(String line) {

        line = line.replaceAll("\"","");// pominie nam wszystkie cudzysłowia z pliku tekstowego

        splitFeedInfo = line.split(FeedParser.DATA_DELIMITER, -1);

        return null;
    }

    @Override
    public String parseFrom(FeedModel feed)
    {
        throw new UnsupportedOperationException("not implemented yet! ");  //rzucamy wyjątkiem

    }


}
