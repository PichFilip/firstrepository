package core.parser;

import core.model.Agency;

public class AgencyParser extends BaseFeedParser<Agency> {

    private static final int AGENCY_NAME_COLUME = 0;
    private static final int AGENCY_URL_COLUME = 1;
    private static final int AGENCY_TIMEZONE_COLUME = 2;
    private static final int AGENCY_LANG_COLUME = 3;
    private static final int AGENCY_PHONE_COLUME = 4;



    @Override
    public Agency parseTo(String feedInfo) {
        super.parseTo(feedInfo);
        Agency agency = new Agency();
        agency.setAgencyName(splitFeedInfo[AGENCY_NAME_COLUME]);
        agency.setAgencyUrl(splitFeedInfo[AGENCY_URL_COLUME]);
        agency.setAgencyTimeZone(splitFeedInfo[AGENCY_TIMEZONE_COLUME]);
        agency.setAgencyLang(splitFeedInfo[AGENCY_LANG_COLUME]);
        agency.setAgencyPhone(splitFeedInfo[AGENCY_PHONE_COLUME]);

        return agency;
    }

}
