package core.parser;

import core.model.Stops;


public class StopsParser extends BaseFeedParser<Stops> {

    private static final int STOP_ID_COLUMNE = 0;
    private static final int STOP_CODE__COLUMNE = 1;
    private static final int STOP_NAME_COLUMNE = 2;
    private static final int STOP_DESC_COLUMNE = 3;
    private static final int STOP_LAT_COLUMNE = 4;
    private static final int STOP_LON_COLUMNE = 5;
    private static final int STOP_URL_COLUMNE = 6;
    private static final int LOCATION_TYPE_COLUMNE = 7;
    private static final int PARENT_STATION_COLUMNE = 8;
    private static final int WHEELCHAIR_BOARDING_COLUMNE = 9;

    @Override
    public Stops parseTo(String line) {
        super.parseTo(line);
        Stops stops = new Stops();
        stops.setStopId(splitFeedInfo[STOP_ID_COLUMNE]);
        stops.setStopCode(splitFeedInfo[STOP_CODE__COLUMNE]);
        stops.setStopName(splitFeedInfo[STOP_NAME_COLUMNE]);
        stops.setStopDesc(splitFeedInfo[STOP_DESC_COLUMNE]);
        stops.setStopLat(splitFeedInfo[STOP_LAT_COLUMNE]);
        stops.setStopLon(splitFeedInfo[STOP_LON_COLUMNE]);
        stops.setStopUrl(splitFeedInfo[STOP_URL_COLUMNE]);
        stops.setLocationType(splitFeedInfo[LOCATION_TYPE_COLUMNE]);
        stops.setParentStation(splitFeedInfo[PARENT_STATION_COLUMNE]);
        stops.setWheelChairAccessible(Integer.parseInt(splitFeedInfo[WHEELCHAIR_BOARDING_COLUMNE]) == 1);

        return stops;
    }

}
