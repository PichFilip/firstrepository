package core.parser;

import core.model.Calendar;
import core.reader.BaseFeedReader;

public class CalendarParser extends BaseFeedParser<Calendar>{
    //service_id,monday,tuesday,wednesday,thursday,friday,saturday,sunday,start_date,end_date

    private final static int SERVICE_ID_COLUMNE = 0;
    private final static int MONDAY_COLUMNE = 1;
    private final static int TUESDAY_COLUMNE = 2;
    private final static int WEDNESDAY_COLUMNE = 3;
    private final static int THURSDAY_COLUMNE = 4;
    private final static int FRIDAY_COLUMNE = 5;
    private final static int SATURDAY_COLUMNE = 6;
    private final static int SUNDAY_COLUMNE = 7;
    private final static int STARTY_DATE_COLUMNE = 8;
    private final static int END_DATE_COLUMNE = 9;

    @Override
    public Calendar parseTo(String line) {
        super.parseTo(line);
        Calendar calendar = new Calendar();
        calendar.setServiceId(splitFeedInfo[SERVICE_ID_COLUMNE]);
        calendar.setMondayRuns(Integer.parseInt(splitFeedInfo[MONDAY_COLUMNE]) == 1);
        calendar.setTuesdayRuns(Integer.parseInt(splitFeedInfo[TUESDAY_COLUMNE]) == 1);
        calendar.setWednesdayRuns(Integer.parseInt(splitFeedInfo[WEDNESDAY_COLUMNE] ) == 1);
        calendar.setThursdayRuns(Integer.parseInt(splitFeedInfo[THURSDAY_COLUMNE]) == 1);
        calendar.setFridayRuns(Integer.parseInt(splitFeedInfo[FRIDAY_COLUMNE]) == 1);
        calendar.setSaturdayRuns(Integer.parseInt(splitFeedInfo[SATURDAY_COLUMNE]) == 1);
        calendar.setSundayRuns(Integer.parseInt(splitFeedInfo[SUNDAY_COLUMNE]) == 1);
        calendar.setStartDate(splitFeedInfo[STARTY_DATE_COLUMNE]);
        calendar.setEndDate(splitFeedInfo[END_DATE_COLUMNE]);

        return  calendar;

    }
}
