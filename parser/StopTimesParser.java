package core.parser;

import core.model.PickupType;
import core.model.StopTimes;
import core.reader.BaseFeedReader;

public class StopTimesParser extends BaseFeedParser<StopTimes>{
    private static final int TRIP_ID_COLUMNE = 0;
    private static final int ARRIVAL_COLUMNE = 1;
    private static final int ARRIVAL_TIME_COLUMNE = 2;
    private static final int DEPARTURE_TIME_COLUMNE = 3;
    private static final int STOP_ID_COLUMNE = 4;
    private static final int STOP_SEQUENCE_COLUMNE = 5;
    private static final int STOP_HEADSIN_COLUMNE = 6;
    private static final int PICKUP_TYPE_COLUMNE = 7;
    private static final int DROP_OFF_TYPE_COLUMNE = 8;

    @Override
    public StopTimes parseTo(String line) {
        super.parseTo(line);
        StopTimes stopTimes = new StopTimes();
        stopTimes.setTripId(splitFeedInfo[TRIP_ID_COLUMNE]);
        stopTimes.setArrival(splitFeedInfo[ARRIVAL_COLUMNE]);
        stopTimes.setArrivalTime(splitFeedInfo[ARRIVAL_TIME_COLUMNE]);
        stopTimes.setDepartueTime(splitFeedInfo[DEPARTURE_TIME_COLUMNE]);
        stopTimes.setStopId(splitFeedInfo[STOP_ID_COLUMNE]);
        stopTimes.setStopSequence(splitFeedInfo[STOP_SEQUENCE_COLUMNE]);
        stopTimes.setStopHeadsign(splitFeedInfo[STOP_HEADSIN_COLUMNE]);
        stopTimes.setPickupType(PickupType.getType(Integer.parseInt(splitFeedInfo[PICKUP_TYPE_COLUMNE])));
        stopTimes.setDropOffType(PickupType.getType(Integer.parseInt(splitFeedInfo[DROP_OFF_TYPE_COLUMNE])));
        return stopTimes;
    }
}
