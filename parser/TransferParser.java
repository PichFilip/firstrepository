package core.parser;

import core.model.TransferType;
import core.model.Transfers;

public class TransferParser extends BaseFeedParser<Transfers> {
    //from_stop_id,to_stop_id,transfer_type,min_transfer_time
    private static final int FROM_STOP_ID_COLUMNE = 0;
    private static final int TO_STOP_ID_COLUMNE = 1;
    private static final int TRANSFER_TYPE_COLUMNE = 2;
    private static final int MIN_TRANSFER_TIME = 3;

    @Override
    public Transfers parseTo(String line) {
        super.parseTo(line);
        Transfers transfers = new Transfers();
        transfers.setFromStopId(splitFeedInfo[FROM_STOP_ID_COLUMNE]);
        transfers.setToStopId(splitFeedInfo[TO_STOP_ID_COLUMNE]);
        transfers.setTransferType(TransferType.getType(Integer.parseInt(splitFeedInfo[TRANSFER_TYPE_COLUMNE])));
        transfers.setTransferTime(splitFeedInfo[MIN_TRANSFER_TIME]);
        return transfers;
    }
}
