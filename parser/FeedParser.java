package core.parser;

import core.model.FeedModel;

public interface FeedParser <T extends FeedModel> {   // T jets parametrem generycznym, dowolny obiekt spełniający warunek

    public static final String DATA_DELIMITER = ",";  // ustalamy, że przecienek jest naszym dzielnikiem lini

    T parseTo(String feedInfo);

    String parseFrom(T feed);


}
