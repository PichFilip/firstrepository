package core.service;

import core.model.FeedModel;
import core.model.Trip;
import core.reader.FeedReader;
import core.repository.FeedRepository;

import java.util.ArrayList;
import java.util.List;

public class TripService implements TripAPI<FeedModel> {

    private final FeedRepository<Trip> tripFeedRepository;
    private final FeedReader<Trip> tripFeedReader;

    public TripService(FeedRepository<Trip> tripFeedRepository, FeedReader<Trip> tripFeedReader) {
        this.tripFeedRepository = tripFeedRepository;
        this.tripFeedReader = tripFeedReader;
    }

    @Override
    public void loadTrips() {
        tripFeedReader.read();
    }

    @Override
    public List<Trip> findTripByRouteId(String routeId) {
        List<Trip> routeTrips = new ArrayList<>();
        for (Trip trip : tripFeedRepository.getAll()) {
            if (trip.getRouteId().equals(routeId)) ;
            routeTrips.add(trip);
        }
        return routeTrips;
    }

    @Override
    public List<Trip> getAllTrips() {
        return tripFeedRepository.getAll();
    }

    @Override
    public List<Trip> findTripByTripHeadsign(String tripHeadSign) {
        List<Trip> headTrips = new ArrayList<>();
        for (Trip trip : tripFeedRepository.getAll()) {
            if (trip.getTripHeadsign().equals(tripHeadSign)) ;
            headTrips.add(trip);
        }
        return headTrips;
    }

    @Override
    public List<Trip> findTripByBoth(String routeId, String tripHeadSign) {
        List<Trip> headrouteTrips = new ArrayList<>();
        for (Trip trip : tripFeedRepository.getAll()) {
            if (trip.getTripHeadsign().equals(tripHeadSign) && trip.getRouteId().equals(routeId)) ;
            headrouteTrips.add(trip);
        }
        return headrouteTrips;
    }

}
