package core.service;

import core.model.FeedModel;
import core.model.Trip;

import java.util.List;

public interface TripAPI<T extends FeedModel> {
    void loadTrips();
    List<Trip> findTripByRouteId(String routeId);
    List<Trip> getAllTrips();
    List<Trip> findTripByTripHeadsign(String tripHeadSign);
    List<Trip> findTripByBoth(String routeId, String tripHeadSign);

}
