package core.view;

import controller.TripController;

public interface TripView {
    void loadPage();
    void setController(TripController tripController);
}
