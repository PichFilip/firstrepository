package core.reader;

import core.model.Shapes;
import core.parser.FeedParser;
import core.repository.FeedRepository;

public class ShapeFeedReader extends BaseFeedReader<Shapes> {
    public ShapeFeedReader(FeedParser<Shapes> parser, FeedRepository<Shapes> repository) {
        super(parser, repository);
    }

    @Override
    public String getFileDir() {
        return "./resources/shapes.txt";
    }
}
