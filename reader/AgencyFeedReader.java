package core.reader;

import core.model.Agency;
import core.parser.FeedParser;
import core.repository.FeedRepository;

public class AgencyFeedReader extends BaseFeedReader<Agency> {

    public AgencyFeedReader(FeedParser<Agency> parser, FeedRepository<Agency> repository) {
        super(parser, repository);
    }

    @Override
    public String getFileDir() {
        return "./resources/agency.txt";
    }
}
