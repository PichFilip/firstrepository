package core.reader;

import core.model.Route;
import core.parser.BaseFeedParser;
import core.parser.FeedParser;
import core.repository.FeedRepository;

public class RouteFeedReader extends BaseFeedReader<Route> {

    public RouteFeedReader(FeedParser<Route> parser, FeedRepository<Route> repository) {
        super(parser, repository);
    }

    @Override
    public String getFileDir() {
        return "./resources/routes.txt";
    }

}
