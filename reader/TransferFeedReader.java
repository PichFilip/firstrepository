package core.reader;

import core.model.Transfers;
import core.parser.FeedParser;
import core.repository.FeedRepository;

public class TransferFeedReader extends BaseFeedReader<Transfers> {
    public TransferFeedReader(FeedParser<Transfers> parser, FeedRepository<Transfers> repository) {
        super(parser, repository);
    }

    @Override
    public String getFileDir() {
        return "./resources/transfers.txt";
    }
}
