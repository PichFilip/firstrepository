package core.reader;

import core.model.CalendarDates;
import core.parser.FeedParser;
import core.repository.FeedRepository;

public class CalendarDateFeedReader extends BaseFeedReader<CalendarDates>{

    public CalendarDateFeedReader(FeedParser<CalendarDates> parser, FeedRepository<CalendarDates> repository) {
        super(parser, repository);
    }

    @Override
    public String getFileDir() {
        return "./resources/calendar_dates.txt";
    }
}
