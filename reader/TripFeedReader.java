package core.reader;

import core.model.Trip;
import core.parser.FeedParser;
import core.repository.FeedRepository;

public class TripFeedReader extends BaseFeedReader<Trip> {


    public TripFeedReader(FeedParser<Trip> parser, FeedRepository<Trip> repository) {
        super(parser, repository);
    }

    @Override
    public String getFileDir() {
        return "./resources/trips.txt";
    }
}
