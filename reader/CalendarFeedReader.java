package core.reader;

import core.model.Calendar;
import core.parser.FeedParser;
import core.repository.FeedRepository;

public class CalendarFeedReader extends BaseFeedReader<Calendar> {

    public CalendarFeedReader(FeedParser<Calendar> parser, FeedRepository<Calendar> repository) {
        super(parser, repository);

    }

    @Override
    public String getFileDir() {
        return "./resources/calendar.txt";
    }
}
