package core.reader;

import core.model.Stops;
import core.parser.FeedParser;
import core.repository.FeedRepository;

public class StopsFeedReader extends BaseFeedReader<Stops> {

    public StopsFeedReader(FeedParser<Stops> parser, FeedRepository<Stops> repository) {
        super(parser, repository);
    }

    @Override
    public String getFileDir() {
        return "./resources/stops.txt";
    }
}
