package core.reader;

import core.model.FeedModel;

public interface FeedReader<T extends FeedModel>
{
    void read();
    String getFileDir();

}
