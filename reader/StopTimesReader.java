package core.reader;

import core.model.StopTimes;

import core.parser.FeedParser;
import core.repository.FeedRepository;

public class StopTimesReader extends BaseFeedReader<StopTimes> {
    public StopTimesReader(FeedParser<StopTimes> parser, FeedRepository<StopTimes> repository) {
        super(parser, repository);
    }

    @Override
    public String getFileDir() {
        return "./resources/stop_times.txt";
    }
}
