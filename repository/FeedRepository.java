package core.repository;

import core.model.Agency;
import core.model.FeedModel;
import java.util.List;
public interface FeedRepository <T extends FeedModel>{
    void add(T feed);
    T get(String id);
    void update(T feed);
    void delete(String id);
    void delete(T feed);
    List<T> getAll();
    void insert(T feed);
}